//
//  File.swift
//  
//
//  Created by Home on 17/10/19.
//

import Foundation

public struct Status: Decodable {
    public let version: String
    public let health: Int
}
